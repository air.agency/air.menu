<?php
namespace Air\Menu\Model;

use Air\Core\Api\Model\FilterInterface;
use Air\Core\Api\Model\Traits;

class MenuListFilter implements FilterInterface
{
    use Traits\IdsTrait;
    use Traits\TitleTrait;
    use Traits\CodeTrait;
    use Traits\TimestampableTrait;
}
