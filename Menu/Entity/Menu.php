<?php

namespace Air\Menu\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Air\Core\Entity\Traits;
use Air\Core\Entity\AbstractEntity;

 /**
 * @ORM\Entity()
 * @ORM\Table(
 *     name="air_app_menu_menu",
 *     indexes={
 *         @ORM\Index(name="air_app_menu_menu_published_idx", columns={"published"}),
 *         @ORM\Index(name="air_app_menu_menu_code_published_idx", columns={"code", "published"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="Air\Menu\Repository\MenuRepository")
 * @Gedmo\SoftDeleteable(fieldName="systemDeletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("code")
 */
class Menu extends AbstractEntity
{
    use Traits\TitleTrait;
    use Traits\CodeUniqueTrait;
    use Traits\PublishedTrait;
    use Traits\DescriptionTrait;

    /**
     * Items
     *
     * @var Model[]|ArrayCollection
     *
     * @Serializer\Type("ArrayCollection<Air\Menu\Entity\Item>")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(1)
     *
     * @ORM\OneToMany(targetEntity="Item", mappedBy="menu", cascade={"persist"})
     *
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getItems():Collection
    {
        return $this->items;
    }

    public function addItem(Item $item):self
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }
        return $this;
    }

    /**
     * @param Collection $items
     * @return self
     */
    public function setItems(Collection $items):self
    {
        $this->items = $items;
        return $this;
    }

}
