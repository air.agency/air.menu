<?php

namespace Air\Menu\Entity;

use Air\Core\Entity\Media\Traits\FileTrait;
use Air\Core\Entity\Media\Traits\IconTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Air\Core\Entity\Traits;
use Symfony\Component\Validator\Constraints as Assert;
use Air\Core\Entity\AbstractEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     name="air_app_menu_item",
 *     indexes={
 *         @ORM\Index(name="air_app_menu_item_published_idx", columns={"published"})
 *     },
 *    uniqueConstraints={
 *        @ORM\UniqueConstraint(name="air_app_menu_item_code_menu_idx", columns={"code", "menu_id"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="Air\Menu\Repository\ItemRepository")
 * @Gedmo\SoftDeleteable(fieldName="systemDeletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 */
class Item extends AbstractEntity
{
    use Traits\TitleTrait;
    use Traits\CodeEmptyTrait;
    use Traits\PublishedTrait;
    use Traits\UrlTrait;
    use Traits\OrderingTrait;
    use Traits\DescriptionTrait;
    use IconTrait;

    /**
     * Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Assert\NotBlank
     */
    protected ?Menu $menu = null;

    /**
     * @var Parent
     *
     * @Serializer\Type("Air\Menu\Entity\Item")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(1)
     *
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     * @ORM\JoinColumn(nullable=true)
     */
    protected ?Item $parent;

    /**
     * Sub items
     *
     * @var Model[]|ArrayCollection
     *
     * @Serializer\Type("ArrayCollection<Air\Menu\Entity\Item>")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(1)
     *
     * @ORM\OneToMany(targetEntity="Item", mappedBy="parent", cascade={"persist"})
     *
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("parentId")
     * @Serializer\Groups({"list", "view"})
     */
    public function getParentId(): ?int
    {
        return $this->parent ? $this->parent->getId() : null;
    }

    /**
     * @return Item|null
     */
    public function getParent(): ?Item
    {
        return $this->parent;
    }

    /**
     * @param Item $parent
     */
    public function setParent(?Item $parent = null): Item
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @param Menu|null $menu
     * @return self
     */
    public function setMenu(?Menu $menu): Item
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * @return Menu
     */
    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $item->setParent($this);
            $item->setMenu($this->getMenu());

            $this->items->add($item);
        }

        return $this;
    }

    /**
     * @param Collection $items
     * @return self
     */
    public function setItems(Collection $items): self
    {
        $this->items = $items;
        return $this;
    }
}
