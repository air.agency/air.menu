<?php

namespace Air\Menu\Tests\Controller;

use Air\Menu\Entity\Menu;
use App\Tests\Pipeline\AbstractTestForApiGetItemMethod;

class MenuViewTest extends AbstractTestForApiGetItemMethod
{
    protected function createEntity(): Menu
    {
        $loader    = self::$container->get('nelmio_alice.file_loader');
        $objectSet = $loader->loadFile(self::$kernel->getProjectDir() . '/src/Menu/Fixtures/Entity/MenuView.yaml');

        return $objectSet->getObjects()['test1'];
    }

    /**
     * @param News $entity
     *
     * @return string
     */
    protected function createApiUrlByEntity($entity): string
    {
        return '/api/v1/menu/' . $entity->getCode() . '/view';
    }

    /**
     * @param News $entity
     */
    protected function modifyEntity($entity): void
    {
        $entity->setTitle('Changed title');
    }
}

