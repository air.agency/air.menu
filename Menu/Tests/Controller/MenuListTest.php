<?php

namespace Air\Menu\Tests\Controller;

use App\Tests\Pipeline\AbstractTestForApiGetItemListMethod;

class MenuListTest extends AbstractTestForApiGetItemListMethod
{
    protected function createApiUrl(): string
    {
        return '/api/v1/menu/list';
    }

    protected function getFixturesFiles(): array
    {
        return [
            self::$kernel->getProjectDir() . '/src/Menu/Fixtures/Entity/Menu.yaml',
        ];
    }
}

