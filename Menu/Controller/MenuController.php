<?php

namespace Air\Menu\Controller;

use Air\Core\Api\Controller\BaseController;
use Air\Core\Api\Form\PaginationFormType;
use Air\Core\Api\Model\Pagination as PaginationModel;
use Air\Core\Api\Service\Action\CachedAction;
use Air\Core\Api\Service\Action\CachedResponse;
use Air\Core\Service\Cache\CacheHelper;
use Air\Menu\Entity\Menu;
use Air\Menu\Form\MenuFilterType;
use Air\Menu\Model\MenuListFilter;
use Air\Menu\Response\MenuListResponse;
use Air\Menu\Response\MenuViewResponse;
use Air\Menu\Service\MenuManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sandronimus\NelmioFormGetParameterPluginBundle\Annotation\FormGetParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

/**
 * @Route("/api/v1/menu")
 */
class MenuController extends BaseController
{
    /**
     * Menu view
     *
     * @SWG\Tag(name="Menu")
     * @SWG\Get(security={}, summary="Menu view", description="Menu view")
     * @SWG\Response(
     *    response="200",
     *    description="Menu view",
     *    @SWG\Schema(
     *        ref=@Model(type="Air\Menu\Response\MenuViewResponse", groups={"view"})
     *    )
     * )
     * @SWG\Response(response="401", description="Bad credentials")
     *
     * @Route("/{code}/view", name="api.v1.menu.view", methods={"GET"})
     * @CachedAction()
     */
    public function menuViewAction(Request $request, MenuManager $menuManager, $code)
    {
        $menu = $menuManager->getByCode($code);
        if (!$menu) {
            return $this->errorAnswer(["Menu not found"], ['view'], Response::HTTP_NOT_FOUND);
        }

        $response = new MenuViewResponse($menu);

        $tags = [
            CacheHelper::getTagByClass(Menu::class),
            CacheHelper::getTagByEntityList([$response->getData()]),
        ];

        $groups = ['view'];

        return new CachedResponse($response, $tags, $groups);
    }

    /**
     * Menu list
     *
     * @SWG\Tag(name="Menu")
     * @SWG\Get(security={}, summary="Menu list", description="Menu list")
     * @FormGetParameter(formType=MenuFilterType::class)
     * @FormGetParameter(formType=PaginationFormType::class)
     * @SWG\Response(
     *    response="200",
     *    description="Menu list",
     *    @SWG\Schema(
     *        ref=@Model(type="Air\Menu\Response\MenuListResponse", groups={"list"})
     *    )
     * )
     * @SWG\Response(response="401", description="Bad credentials")
     *
     * @Route("/list", name="api.v1.menu.list", methods={"GET"})
     * @CachedAction()
     */
    public function menuListAction(Request $request, MenuManager $menuManager)
    {
        $paginationForm = $this->createForm(PaginationFormType::class, new PaginationModel());
        $paginationForm->handleRequest($request);

        $filterForm = $this->createForm(MenuFilterType::class, new MenuListFilter());
        $filterForm->handleRequest($request);

        $errors = $this->validateForms([$paginationForm, $filterForm]);
        if ($errors) {
            return $this->errorAnswer($errors);
        }

        $dataPaginator = $menuManager->listPaginate($filterForm->getData(), $paginationForm->getData());

        $response = new MenuListResponse($dataPaginator, $paginationForm->getData());

        $tags = [
            CacheHelper::getTagByClass(Menu::class),
            CacheHelper::getTagByEntityList($response->getData()),
        ];

        $groups = ['list'];

        return new CachedResponse($response, $tags, $groups);
    }
}
