<?php

namespace Air\Menu\Admin;

use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Air\Core\Admin\AdminHelper;
use Sonata\AdminBundle\Form\Type\ModelListType;


class ItemAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->tab('Main')
            ->with(' ')
            ->add('menu', ModelListType::class, [
                'required' => false,
                'btn_edit' => false,
                'btn_add' => false,
            ])
            ->add('title', TextType::class)
            ->add('code', TextType::class)
            ->add('url', TextareaType::class)
            ->add('published', CheckboxType::class, ['required' => false])
            ->add('parent', ModelListType::class, [
                'required' => false,
                'btn_edit' => false,
                'btn_add' => false,
            ])
            ->add('icon', ModelListType::class, ['required' => false], ['link_parameters' => ['context' => 'icon']])
            ->add('ordering', TextareaType::class)
            ->end()
            ->end()
        ;
        $form = AdminHelper::addSystemInfo($form);
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('menu')
            ->add('parent')
            ->add('code')
            ->add('published')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                )
            ))
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('menu')
            ->add('parent')
            ->add('title')
            ->add('code')
            ->add('published')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('menu')
            ->add('parent')
            ->add('title')
            ->add('code')
            ->add('published')
        ;
    }
}
