<?php

namespace Air\Menu\Admin;

use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Air\Core\Admin\AdminHelper;
use Sonata\Form\Type\CollectionType;

class MenuAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->tab('Main')
            ->with(' ')
                ->add('title', TextType::class)
                ->add('code', TextType::class)
                ->add('published', CheckboxType::class, ['required' => false])
            ->end()
            ->end()
            ->tab('Items')
                ->with(' ')
                    ->add('items', CollectionType::class, [
                        'type_options' => [
                            'delete' => false,
                        ],
                        'required' => false,
                        'by_reference' => true,
                        'block_name' => false,
                    ], [
                        'btn_delete' => false,
                        'block_name' => '',
                        'allow_add' => true,
                        'edit' => 'inline',
                        'inline' => 'table',
                        'admin_code' => 'Air\Menu\Admin\ItemTableAdmin'
                    ])
                ->end()
            ->end()
        ;

        $form = AdminHelper::addSystemInfo($form);
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('code')
            ->add('published')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                )
            ))
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('title')
            ->add('code')
            ->add('published')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('code')
            ->add('published')
        ;
    }
}
