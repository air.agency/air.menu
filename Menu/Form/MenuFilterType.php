<?php

namespace Air\Menu\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Air\Menu\Model\MenuListFilter;
use Air\Core\Api\Form\AbstractFilterType;
use Air\Core\Api\Form\Traits;

class MenuFilterType extends AbstractFilterType
{
    use Traits\TitleTrait;
    use Traits\CodeTrait;
    use Traits\IdsTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = $this->addIds($builder);
        $builder = $this->addTitle($builder);
        $builder = $this->addCode($builder);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method' => 'GET',
            'data_class'    => MenuListFilter::class,
        ]);
    }
}
