<?php

namespace Air\Menu\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Air\Menu\Model\MenuListFilter;
use Air\Core\Api\Model\Pagination;
use Air\Menu\Entity\Menu;

class MenuManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function getByCode(string $code):?Menu
    {
        return $this->entityManager->getRepository(Menu::class)->getPublishedByCode($code);
    }

    public function getById(int $id):?Menu
    {
        return $this->entityManager->getRepository(Menu::class)->getPublishedById($id);
    }

    public function listPaginate(MenuListFilter $filter, ?Pagination $pagination, $sort = null):Paginator
    {
        return $this->entityManager->getRepository(Menu::class)->listPaginate($filter, $pagination, $sort);
    }
}
