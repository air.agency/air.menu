<?php

namespace Air\Menu\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\QueryBuilder;
use Air\Menu\Model\MenuListFilter;
use Air\Menu\Entity\Menu;
use Air\Core\Api\Model\Pagination;

class MenuRepository extends EntityRepository
{
    public function getPublishedQb():QueryBuilder
    {
        $qb = $this->createQueryBuilder('menu');
        $qb
            ->leftJoin('menu.items', 'menuItems')
            ->andWhere($qb->expr()->eq('menu.published', ':published'))
            ->setParameter('published', true)
        ;

        return $qb;
    }

    public function getPublishedByCode(string $code):?Menu
    {
        $qb = $this->getPublishedQb();

        $qb
            ->andWhere($qb->expr()->eq('menu.code', ':code'))
            ->setParameter('code', $code)
            ->setMaxResults(1)
        ;

        return  $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @inheritDoc
     */
    public function listPaginate(MenuListFilter $filter, ?Pagination $pagination, $sort = null): Paginator
    {
        $queryBuilder = $this->getPublishedQb();

        if ($filter->getTitle()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->like('menu.title', ':title'))
                ->setParameter('title', '%'.$filter->getTitle().'%')
            ;
        }

        if ($filter->getCode()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq('menu.code', ':code'))
                ->setParameter('code', $filter->getCode())
            ;
        }

        if ($filter->getIds()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('menu.id', ':ids'))
                ->setParameter('ids', $filter->getIds())
            ;
        }

        if ($pagination) {
            $queryBuilder
                ->setFirstResult(($pagination->getPage() - 1) * $pagination->getLimit())
                ->setMaxResults($pagination->getLimit())
            ;
        }

        return new Paginator($queryBuilder);
    }
}
