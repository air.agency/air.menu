<?php

namespace Air\Menu\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\QueryBuilder;

use Air\Menu\Entity\Page;
use Air\Menu\Model\PageListFilter;
use Air\Core\Api\Model\Pagination;

use Symfony\Component\Form\Exception\OutOfBoundsException;

class ItemRepository extends EntityRepository
{
    public function getPublishedQb():QueryBuilder
    {
        $qb = $this->createQueryBuilder('menuItem');
        $qb
            ->leftJoin('menuItem.menu', 'menu')
            ->andWhere($qb->expr()->eq('menu.published', ':published'))
            ->andWhere($qb->expr()->eq('menuItem.published', ':published'))
            ->setParameter('published', true)
        ;

        return $qb;
    }

    /**
     * @inheritDoc
     */
    public function listPaginate(PageListFilter $filter, ?Pagination $pagination, $sort = null): Paginator
    {
        $queryBuilder = $this->getPublishedQb();

        if ($filter->getTitle()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->like('menuItem.title', ':title'))
                ->setParameter('title', '%'.$filter->getTitle().'%')
            ;
        }

        if ($filter->getCode()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq('menuItem.code', ':code'))
                ->setParameter('code', $filter->getCode())
            ;
        }

        if ($filter->getIds()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('menuItem.id', ':ids'))
                ->setParameter('ids', $filter->getIds())
            ;
        }

        if ($pagination) {
            $queryBuilder
                ->setFirstResult(($pagination->getPage() - 1) * $pagination->getLimit())
                ->setMaxResults($pagination->getLimit())
            ;
        }

        return new Paginator($queryBuilder);
    }
}
