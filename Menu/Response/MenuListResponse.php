<?php

namespace Air\Menu\Response;

use Air\Core\Api\Response\AbstractListResponse;
use JMS\Serializer\Annotation as Serializer;

class MenuListResponse extends AbstractListResponse
{
    /**
     * @var array
     *
     * @Serializer\Type("array<Air\Menu\Entity\Menu>")
     * @Serializer\Groups({"list"})
     */
    public $data;
}
