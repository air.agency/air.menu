<?php

namespace Air\Menu\Response;

use JMS\Serializer\Annotation as Serializer;
use Air\Core\Api\Response\AbstractResponse;

class MenuViewResponse extends AbstractResponse
{
    /**
     * @var Menu
     *
     * @Serializer\Type("Air\Menu\Entity\Menu")
     * @Serializer\Groups({"view"})
     */
    public $data;
}
