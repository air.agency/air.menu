<?php

namespace Air\Menu\Response;

use JMS\Serializer\Annotation as Serializer;
use Air\Core\Api\Response\AbstractListResponse;

class ItemListResponse extends AbstractListResponse
{
    /**
     * @var array
     *
     * @Serializer\Type("array<Air\Menu\Entity\Item>")
     * @Serializer\Groups({"list"})
     */
    public $data;
}
